use crate::register::*;
use crate::memory::*;
use crate::gameboy::*;

use std::sync::{
    Arc,
    Mutex,
};

#[derive(Clone, Copy)]
pub enum CPUState {
    Running,
    Hang,
    HALT,
    STOP,
}

pub struct CPU {
    pub registers: Registers,
    pub memory: Arc<Mutex<Memory>>,
    pub running_state: CPUState,
    pub interrupts_enabled: bool,
    pub clock: Arc<Mutex<Clock>>,
}

impl CPU {
    pub fn new(memory: Arc<Mutex<Memory>>, clock: Arc<Mutex<Clock>>) -> CPU {
        CPU {
            registers: Registers::new(),
            memory: memory,
            running_state: CPUState::Running,
            interrupts_enabled: true,
            clock: clock,
        }
    }

    pub fn set_pc(&mut self, pc: u16) {
        self.registers.set16(Register16::PC, pc);
    }

    pub fn step_forward(&mut self) -> CPUState {
        let time = self.execute_instruction();
        let mut clock = self.clock.lock().unwrap();
        clock.increment(time);
        self.running_state
    }

    /// Execute the next instruction, return m- and t-times consumed
    fn execute_instruction(&mut self) -> u64 {
        let mut memory = self.memory.lock().unwrap();

        let instruction = memory.read_byte(
            self.registers.bump_pc());

        macro_rules! ld {
            ((HL), d8) => {{ //immediate data to (HL)
                let data = memory.read_byte(self.registers.bump_pc());
                memory.write_byte(self.registers.get16(Register16::HL), data);
                12
            }};
            ((HL+),A) => {{
                let data = self.registers.get8(Register8::A);
                memory.write_byte(self.registers.get16(Register16::HL), data);
                let hl = self.registers.get16(Register16::HL);
                self.registers.set16(Register16::HL, hl.wrapping_add(1));
                8
            }};
            ((HL-),A) => {{
                let data = self.registers.get8(Register8::A);
                memory.write_byte(self.registers.get16(Register16::HL), data);
                let hl = self.registers.get16(Register16::HL);
                self.registers.set16(Register16::HL, hl.wrapping_sub(1));
                8
            }}; 
            (A,(HL+)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                self.registers.set8(Register8::A, data);
                let hl = self.registers.get16(Register16::HL);
                self.registers.set16(Register16::HL, hl.wrapping_add(1));
                8
            }};
            (A,(HL-)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                self.registers.set8(Register8::A, data);
                let hl = self.registers.get16(Register16::HL);
                self.registers.set16(Register16::HL, hl.wrapping_sub(1));
                8
            }}; 
            ((a16),A) => {{
                let addr = memory.read_word(self.registers.bump_pc_word());
                memory.write_byte(addr, self.registers.get8(Register8::A));
                16
            }};
            (A,(a16)) => {{
                let addr = memory.read_word(self.registers.bump_pc_word());
                self.registers.set8(Register8::A, memory.read_byte(addr));
                16
            }};
            (($r2:ident), $r1:ident) => {{ //register to (r2)
                let data = self.registers.get8(Register8::$r1);
                memory.write_byte(self.registers.get16(Register16::HL), data);
                8
            }};
            ($r1:ident, ($r2:ident)) => {{ //(r2) to register
                let data = memory.read_byte(self.registers.get16(Register16::$r2));
                self.registers.set8(Register8::$r1, data);
                8
            }};
            ($r1:ident, d8) => {{ //immediate data to register
                let data = memory.read_byte(self.registers.bump_pc());
                self.registers.set8(Register8::$r1, data);
                8
            }};
            ($r1:ident, $r2:ident) => {{ //register to register
                let data = self.registers.get8(Register8::$r2);
                self.registers.set8(Register8::$r1, data);
                4
            }};
       };

        macro_rules! ld_16_16 {
            ($r1:ident,d16) => {{ //immediate data to register
                let data = memory.read_word(self.registers.bump_pc_word());
                self.registers.set16(Register16::$r1, data);
                12
            }};
        };

        macro_rules! pop {
            ($r1:ident) => {{
                let data = memory.read_word(self.registers.get16(Register16::SP));
                let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                self.registers.set16(Register16::SP, sp);
                self.registers.set16(Register16::$r1, data);
                12
            }};
        };

        macro_rules! push {
            ($r1:ident) => {{
                let data = self.registers.get16(Register16::$r1);
                let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                self.registers.set16(Register16::SP, sp);
                memory.write_word(self.registers.get16(Register16::SP), data);
                16
            }};
        };

        macro_rules! add {
            (A,d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                add!(data, 8)
            }};
            (A,(HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                add!(data, 8)
            }};
            (A, $r2:ident) => {{ //add register to A
                let data = self.registers.get8(Register8::$r2);
                add!(data, 4)
            }};
            (HL, $r2:ident) => {{ //add 16-bit register to HL
                let data = self.registers.get16(Register16::$r2);
                let hl = self.registers.get16(Register16::HL);
                self.registers.set_flag(Flag::C, FlagCondition::If(hl.checked_add(data).is_none()));
                let sum = hl.wrapping_add(data);
                self.registers.set16(Register16::HL, sum);
                self.registers.set_flag(Flag::H, FlagCondition::If((hl&0xFFF) > (sum&0xFFF)));
                8
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::If(a.checked_add($data).is_none()));
                self.registers.set_flag(Flag::H, FlagCondition::If((a&0xF).wrapping_add($data&0xF) < 0xF));
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set8(Register8::A, a.wrapping_add($data));
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::Z, FlagCondition::If(a == 0));
                $cycles
            }};
        };

        macro_rules! sub {
            (d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                sub!(data, 8)
            }};
            ((HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                sub!(data, 8)
            }};
            ($r2:ident) => {{ //subtract register from A
                let data = self.registers.get8(Register8::$r2);
                sub!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::If($data < a));
                self.registers.set_flag(Flag::H, FlagCondition::If(($data&0xF) < (a&0xF)));
                self.registers.set_flag(Flag::N, FlagCondition::Set);
                self.registers.set8(Register8::A, a.wrapping_sub($data));
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::Z, FlagCondition::If(a == 0));
                $cycles
            }};
        };

        macro_rules! cp {
            (d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                cp!(data, 8)
            }};
            ((HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                cp!(data, 8)
            }};
            ($r2:ident) => {{ //subtract register from A
                let data = self.registers.get8(Register8::$r2);
                cp!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::If($data < a));
                self.registers.set_flag(Flag::H, FlagCondition::If(($data&0xF) < (a&0xF)));
                self.registers.set_flag(Flag::N, FlagCondition::Set);
                let a = a.wrapping_sub($data);
                self.registers.set_flag(Flag::Z, FlagCondition::If(a == 0));
                $cycles
            }};
        };

        macro_rules! and {
            (d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                and!(data, 8)
            }};
            ((HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                and!(data, 8)
            }};
            ($r2:ident) => {{ //AND register with A
                let data = self.registers.get8(Register8::$r2);
                and!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::Set);
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set8(Register8::A, a&$data);
                self.registers.set_flag(Flag::Z, FlagCondition::If((a&$data) == 0));
                $cycles
            }};
        };

        macro_rules! or {
            (d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                or!(data, 8)
            }};
            ((HL)) => {{
                let data = memory.read_byte(
                    self.registers.get16(Register16::HL));
                or!(data, 8)
            }};
            ($r2:ident) => {{ //OR register with A
                let data = self.registers.get8(Register8::$r2);
                or!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::Clear);
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set8(Register8::A, a|$data);
                self.registers.set_flag(Flag::Z, FlagCondition::If((a|$data) == 0));
                $cycles
            }};
        };

        macro_rules! xor {
            (d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                xor!(data, 8)
            }};
            ((HL)) => {{
                let data = memory.read_byte(
                    self.registers.get16(Register16::HL));
                xor!(data, 8)
            }};
            ($r2:ident) => {{ //OR register with A
                let data = self.registers.get8(Register8::$r2);
                xor!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::C, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::Clear);
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set8(Register8::A, a^$data);
                self.registers.set_flag(Flag::Z, FlagCondition::If((a^$data) == 0));
                $cycles
            }};
        };

        macro_rules! adc {
            (A,d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                adc!(data, 8)
            }};
            (A,(HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                adc!(data, 4)
            }};
            (A, $r2:ident) => {{ //ADC register to A
                let data = self.registers.get8(Register8::$r2);
                adc!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                let c = if self.registers.get_flag(Flag::C) {1} else {0};
                self.registers.set_flag(Flag::C, FlagCondition::If(a.checked_add($data.wrapping_add(c)).is_none()));
                self.registers.set_flag(Flag::H, FlagCondition::If((a&0xF).wrapping_add(($data&0xF).wrapping_add(c)) < 0xF));
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set8(Register8::A, a.wrapping_add($data).wrapping_add(c));
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::Z, FlagCondition::If(a == 0));
                $cycles
            }};

        };
    
        macro_rules! sbc {
            (A,d8) => {{
                let data = memory.read_byte(self.registers.bump_pc());
                sbc!(data,8)
            }};
            (A,(HL)) => {{
                let data = memory.read_byte(
                    self.registers.get16(Register16::HL));
                sbc!(data,8)
            }};
            (A, $r2:ident) => {{ //SBC register from A
                let data = self.registers.get8(Register8::$r2);
                sbc!(data,4)
            }};
            ($data:ident,$cycles:expr) => {{
                let a = self.registers.get8(Register8::A);
                let c = if self.registers.get_flag(Flag::C) {1} else {0};
                self.registers.set_flag(Flag::C, FlagCondition::If(a.checked_sub($data.wrapping_add(c)).is_none()));
                self.registers.set_flag(Flag::H, FlagCondition::If((a&0xF).wrapping_sub(($data&0xF).wrapping_add(c)) > 0));
                self.registers.set_flag(Flag::N, FlagCondition::Set);
                self.registers.set8(Register8::A, a.wrapping_sub($data.wrapping_add(c)));
                let a = self.registers.get8(Register8::A);
                self.registers.set_flag(Flag::Z, FlagCondition::If(a == 0));
                $cycles
            }};
        };
        
        macro_rules! inc8 {
            ((HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                memory.write_byte(self.registers.get16(Register16::HL), data.wrapping_add(1));
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                inc8!(data, 12)
            }};
            ($reg:ident) => {{
                let data = self.registers.get8(Register8::$reg);
                self.registers.set8(Register8::$reg, data.wrapping_add(1));
                let data = self.registers.get8(Register8::$reg);
                inc8!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                self.registers.set_flag(Flag::Z, FlagCondition::If($data == 0));
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::If($data&0xF == 0));
                4
            }};
        };

        macro_rules! inc16 {
            ($reg:ident) => {{
                let data = self.registers.get16(Register16::$reg);
                self.registers.set16(Register16::$reg, data.wrapping_add(1));
                8
            }};
        };
        
        macro_rules! dec8 {
            ((HL)) => {{
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                memory.write_byte(self.registers.get16(Register16::HL), data.wrapping_sub(1));
                let data = memory.read_byte(self.registers.get16(Register16::HL));
                dec8!(data, 12)
            }};
            ($reg:ident) => {{
                let data = self.registers.get8(Register8::$reg);
                self.registers.set8(Register8::$reg, data.wrapping_sub(1));
                let data = self.registers.get8(Register8::$reg);
                dec8!(data, 4)
            }};
            ($data:ident,$cycles:expr) => {{
                self.registers.set_flag(Flag::Z, FlagCondition::If($data == 0));
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::If($data&0xF == 0xF));
                4
            }};
        };

        macro_rules! dec16 {
            ($reg:ident) => {{
                let data = self.registers.get16(Register16::$reg);
                self.registers.set16(Register16::$reg, data.wrapping_sub(1));
                8
            }};
        };

        match instruction {
            0x00 => 4, //NOP

            0x01 => ld_16_16!(BC,d16),
            0x11 => ld_16_16!(DE,d16),
            0x21 => ld_16_16!(HL,d16),
            0x31 => ld_16_16!(SP,d16),

            0x02 => ld!((BC),A),
            0x12 => ld!((DE),A), 
            0x0A => ld!(A,(BC)),
            0x1A => ld!(A,(DE)),

            0x22 => ld!((HL+),A),
            0x32 => ld!((HL-),A),
            0x2A => ld!(A,(HL+)),
            0x3A => ld!(A,(HL-)),

            0x06 => ld!(B,d8),
            0x0E => ld!(C,d8),
            0x16 => ld!(D,d8),
            0x1E => ld!(E,d8),
            0x26 => ld!(H,d8),
            0x2E => ld!(L,d8),
            0x36 => ld!((HL),d8),
            0x3E => ld!(A,d8),

            0x40 => ld!(B,B),
            0x41 => ld!(B,C),
            0x42 => ld!(B,D),
            0x43 => ld!(B,E),
            0x44 => ld!(B,H),
            0x45 => ld!(B,L),
            0x47 => ld!(B,A),
            0x48 => ld!(C,B),
            0x49 => ld!(C,C),
            0x4A => ld!(C,D),
            0x4B => ld!(C,E),
            0x4C => ld!(C,H),
            0x4D => ld!(C,L),
            0x4F => ld!(C,A),
            0x50 => ld!(D,B),
            0x51 => ld!(D,C),
            0x52 => ld!(D,D),
            0x53 => ld!(D,E),
            0x54 => ld!(D,H),
            0x55 => ld!(D,L),
            0x57 => ld!(D,A),
            0x58 => ld!(E,B),
            0x59 => ld!(E,C),
            0x5A => ld!(E,D),
            0x5B => ld!(E,E),
            0x5C => ld!(E,H),
            0x5D => ld!(E,L),
            0x5F => ld!(E,A),
            0x60 => ld!(H,B),
            0x61 => ld!(H,C),
            0x62 => ld!(H,D),
            0x63 => ld!(H,E),
            0x64 => ld!(H,H),
            0x65 => ld!(H,L),
            0x67 => ld!(H,A),
            0x68 => ld!(L,B),
            0x69 => ld!(L,C),
            0x6A => ld!(L,D),
            0x6B => ld!(L,E),
            0x6C => ld!(L,H),
            0x6D => ld!(L,L),
            0x6F => ld!(L,A),
            0x78 => ld!(A,B),
            0x79 => ld!(A,C),
            0x7A => ld!(A,D),
            0x7B => ld!(A,E),
            0x7C => ld!(A,H),
            0x7D => ld!(A,L),
            0x7F => ld!(A,A),

            0x46 => ld!(B,(HL)),
            0x56 => ld!(D,(HL)),
            0x66 => ld!(H,(HL)),
            0x4E => ld!(C,(HL)),
            0x5E => ld!(E,(HL)),
            0x6E => ld!(L,(HL)),
            0x7E => ld!(A,(HL)),

            0x70 => ld!((HL),B),
            0x71 => ld!((HL),C),
            0x72 => ld!((HL),D),
            0x73 => ld!((HL),E),
            0x74 => ld!((HL),H),
            0x75 => ld!((HL),L),
            0x77 => ld!((HL),A),

            0xEA => ld!((a16),A),
            0xFA => ld!(A,(a16)),

            0x04 => inc8!(B),
            0x0C => inc8!(C),
            0x14 => inc8!(D),
            0x1C => inc8!(E),
            0x24 => inc8!(H),
            0x2C => inc8!(L),
            0x34 => inc8!((HL)),
            0x3C => inc8!(A),

            0x03 => inc16!(BC),
            0x13 => inc16!(DE),
            0x23 => inc16!(HL),
            0x33 => inc16!(SP),

            0x05 => dec8!(B),
            0x0D => dec8!(C),
            0x15 => dec8!(D),
            0x1D => dec8!(E),
            0x25 => dec8!(H),
            0x2D => dec8!(L),
            0x35 => dec8!((HL)),
            0x3D => dec8!(A),

            0x0B => dec16!(BC),
            0x1B => dec16!(DE),
            0x2B => dec16!(HL),
            0x3B => dec16!(SP),

            0xC1 => pop!(BC),
            0xD1 => pop!(DE),
            0xE1 => pop!(HL),
            0xF1 => pop!(AF),

            0xC5 => push!(BC),
            0xD5 => push!(DE),
            0xE5 => push!(HL),
            0xF5 => push!(AF),

            0x80 => add!(A,B),
            0x81 => add!(A,C),
            0x82 => add!(A,D),
            0x83 => add!(A,E),
            0x84 => add!(A,H),
            0x85 => add!(A,L),
            0x87 => add!(A,A),
            0x86 => add!(A,(HL)),
            0xC6 => add!(A,d8),

            0x09 => add!(HL,BC),
            0x19 => add!(HL,DE),
            0x29 => add!(HL,HL),
            0x39 => add!(HL,SP),

            0x90 => sub!(B),
            0x91 => sub!(C),
            0x92 => sub!(D),
            0x93 => sub!(E),
            0x94 => sub!(H),
            0x95 => sub!(L),
            0x97 => sub!(A),
            0x96 => sub!((HL)),
            0xD6 => sub!(d8),

            0xB8 => cp!(B),
            0xB9 => cp!(C),
            0xBA => cp!(D),
            0xBB => cp!(E),
            0xBC => cp!(H),
            0xBD => cp!(L),
            0xBF => cp!(A),
            0xBE => cp!((HL)),
            0xFE => cp!(d8),

            0xA0 => and!(B),
            0xA1 => and!(C),
            0xA2 => and!(D),
            0xA3 => and!(E),
            0xA4 => and!(H),
            0xA5 => and!(L),
            0xA7 => and!(A),
            0xA6 => and!((HL)),
            0xE6 => and!(d8),

            0xB0 => or!(B),
            0xB1 => or!(C),
            0xB2 => or!(D),
            0xB3 => or!(E),
            0xB4 => or!(H),
            0xB5 => or!(L),
            0xB7 => or!(A),
            0xB6 => or!((HL)),
            0xF6 => or!(d8),

            0xA8 => xor!(B),
            0xA9 => xor!(C),
            0xAA => xor!(D),
            0xAB => xor!(E),
            0xAC => xor!(H),
            0xAD => xor!(L),
            0xAF => xor!(A),
            0xAE => xor!((HL)),
            0xEE => xor!(d8),

            0x88 => adc!(A,B),
            0x89 => adc!(A,C),
            0x8A => adc!(A,D),
            0x8B => adc!(A,E),
            0x8C => adc!(A,H),
            0x8D => adc!(A,L),
            0x8F => adc!(A,A),
            0x8E => adc!(A,(HL)),
            0xCE => adc!(A,d8),

            0x98 => sbc!(A,B),
            0x99 => sbc!(A,C),
            0x9A => sbc!(A,D),
            0x9B => sbc!(A,E),
            0x9C => sbc!(A,H),
            0x9D => sbc!(A,L),
            0x9F => sbc!(A,A),
            0x9E => sbc!(A,(HL)),
            0xDE => sbc!(A,d8),

            0xE2 => { //LD (C),A
                let a = self.registers.get8(Register8::A);
                memory.write_byte((self.registers.get8(Register8::C) as u16)+0xFF00, a);
                8
            },
            0xF2 => { //LD A,(C)
                let data = memory.read_byte((self.registers.get8(Register8::C) as u16)+0xFF00);
                self.registers.set8(Register8::A, data);
                8
            },

            0xE0 => { //LDH (a8),A
                let a = self.registers.get8(Register8::A);
                let addr = (memory.read_byte(self.registers.bump_pc()) as u16)+0xFF00;
                memory.write_byte(addr, a);
                12
            },
            0xF0 => { //LDH A,(a8)
                let addr = (memory.read_byte(self.registers.bump_pc()) as u16)+0xFF00;
                self.registers.set8(Register8::A, memory.read_byte(addr));
                12
            },

            0x27 => { //DAA
                let mut a = self.registers.get8(Register8::A);
                if a&0x0F > 0x09 || self.registers.get_flag(Flag::H) { a += 0x06; }
                if a&0xF0 > 0x90 || self.registers.get_flag(Flag::C) {
                    a += 0x60;
                    self.registers.set_flag(Flag::C, FlagCondition::Set);
                }
                self.registers.set8(Register8::A, a);
                let cond = self.registers.get8(Register8::A) == 0;
                self.registers.set_flag(Flag::Z, FlagCondition::If(cond));
                self.registers.set_flag(Flag::H, FlagCondition::Clear);
                4
            },
            0x2F => { //CPL
                let a = self.registers.get8(Register8::A);
                self.registers.set8(Register8::A, !a);
                self.registers.set_flag(Flag::N, FlagCondition::Set);
                self.registers.set_flag(Flag::H, FlagCondition::Set);
                4
            },
            0x37 => { //SCF
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::Clear);
                self.registers.set_flag(Flag::C, FlagCondition::Set);
                4
            },
            0x3F => { //CCF
                let c = self.registers.get_flag(Flag::C);
                self.registers.set_flag(Flag::N, FlagCondition::Clear);
                self.registers.set_flag(Flag::H, FlagCondition::Clear);
                self.registers.set_flag(Flag::C, FlagCondition::If(!c));
                4
            },

            0x10 => { self.running_state = CPUState::STOP; 4 },
            0x76 => { self.running_state = CPUState::HALT; 4 },
            0xF3 => { self.interrupts_enabled = false; 4 },
            0xFB => { self.interrupts_enabled = true; 4 },

            0xC3 => { // JP a16
                let addr = memory.read_byte(self.registers.bump_pc()) as u16;
                self.registers.set16(Register16::PC, addr);
                16
            }

            0xC0 => { // RET NZ
                let z = self.registers.get_flag(Flag::Z);
                if !z {
                    let addr = memory.read_word(self.registers.get16(Register16::SP));
                    let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                    self.registers.set16(Register16::SP, sp);
                    self.registers.set16(Register16::PC, addr);
                    20
                } else {8}
            }
            0xC8 => { // RET Z
                let z = self.registers.get_flag(Flag::Z);
                if z {
                    let addr = memory.read_word(self.registers.get16(Register16::SP));
                    let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                    self.registers.set16(Register16::SP, sp);
                    self.registers.set16(Register16::PC, addr);
                    20
                } else {8}
            }
            0xD0 => { // RET NC
                let c = self.registers.get_flag(Flag::C);
                if !c {
                    let addr = memory.read_word(self.registers.get16(Register16::SP));
                    let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                    self.registers.set16(Register16::SP, sp);
                    self.registers.set16(Register16::PC, addr);
                    20
                } else {8}
            }
            0xD8 => { // RET C
                let c = self.registers.get_flag(Flag::C);
                if c {
                    let addr = memory.read_word(self.registers.get16(Register16::SP));
                    let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                    self.registers.set16(Register16::SP, sp);
                    self.registers.set16(Register16::PC, addr);
                    20
                } else {8}
            }

            0xC9 => { // RET
                let addr = memory.read_word(self.registers.get16(Register16::SP));
                let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                self.registers.set16(Register16::SP, sp);
                self.registers.set16(Register16::PC, addr);
                16
            }
            0xD9 => { // RETI
                let addr = memory.read_word(self.registers.get16(Register16::SP));
                let sp = self.registers.get16(Register16::SP).wrapping_add(2);
                self.registers.set16(Register16::SP, sp);
                self.registers.set16(Register16::PC, addr);
                self.interrupts_enabled = true;
                16
            }

            0xCD => { // CALL a16
                let call_addr = memory.read_word(self.registers.bump_pc_word());
                let return_addr = self.registers.get16(Register16::PC);
                let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                self.registers.set16(Register16::SP, sp);
                memory.write_word(self.registers.get16(Register16::SP), return_addr);
                self.registers.set16(Register16::PC, call_addr);
                24
            }
            0xCC => { // CALL Z,a16
                let z = self.registers.get_flag(Flag::Z);
                if z {
                    let call_addr = memory.read_word(self.registers.bump_pc_word());
                    let return_addr = self.registers.get16(Register16::PC);
                    let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                    self.registers.set16(Register16::SP, sp);
                    memory.write_word(self.registers.get16(Register16::SP), return_addr);
                    self.registers.set16(Register16::PC, call_addr);
                    24
                } else {12}
            }
            0xDC => { // CALL C,a16
                let c = self.registers.get_flag(Flag::C);
                if c {
                    let call_addr = memory.read_word(self.registers.bump_pc_word());
                    let return_addr = self.registers.get16(Register16::PC);
                    let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                    self.registers.set16(Register16::SP, sp);
                    memory.write_word(self.registers.get16(Register16::SP), return_addr);
                    self.registers.set16(Register16::PC, call_addr);
                    24
                } else {12}
            }
            0xC4 => { // CALL NZ,a16
                let z = self.registers.get_flag(Flag::Z);
                if !z {
                    let call_addr = memory.read_word(self.registers.bump_pc_word());
                    let return_addr = self.registers.get16(Register16::PC);
                    let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                    self.registers.set16(Register16::SP, sp);
                    memory.write_word(self.registers.get16(Register16::SP), return_addr);
                    self.registers.set16(Register16::PC, call_addr);
                    24
                } else {12}
            }
            0xD4 => { // CALL NC,a16
                let c = self.registers.get_flag(Flag::C);
                if !c {
                    let call_addr = memory.read_word(self.registers.bump_pc_word());
                    let return_addr = self.registers.get16(Register16::PC);
                    let sp = self.registers.get16(Register16::SP).wrapping_sub(2);
                    self.registers.set16(Register16::SP, sp);
                    memory.write_word(self.registers.get16(Register16::SP), return_addr);
                    self.registers.set16(Register16::PC, call_addr);
                    24
                } else {12}
            }

            0x18 => { // JR r8
                let addr = self.registers.get16(Register16::PC).wrapping_sub(1);
                let offset = memory.read_byte(self.registers.bump_pc()) as i8;
                let addr = addr.wrapping_add(offset as u16);
                self.registers.set16(Register16::PC, addr);
                12
            }
            0x28 => { // JR Z, r8
                let z = self.registers.get_flag(Flag::Z);
                if z {
                    let addr = self.registers.get16(Register16::PC).wrapping_sub(1);
                    let offset = memory.read_byte(self.registers.bump_pc()) as i8;
                    let addr = addr.wrapping_add(offset as u16);
                    self.registers.set16(Register16::PC, addr);
                    12
                } else {8}
            }
            0x38 => { // JR C, r8
                let c = self.registers.get_flag(Flag::C);
                if c {
                    let addr = self.registers.get16(Register16::PC).wrapping_sub(1);
                    let offset = memory.read_byte(self.registers.bump_pc()) as i8;
                    let addr = addr.wrapping_add(offset as u16);
                    self.registers.set16(Register16::PC, addr);
                    12
                } else {8}
            }
            0x20 => { // JR NZ, r8
                let z = self.registers.get_flag(Flag::Z);
                if !z {
                    let addr = self.registers.get16(Register16::PC).wrapping_sub(1);
                    let offset = memory.read_byte(self.registers.bump_pc()) as i8;
                    let addr = addr.wrapping_add(offset as u16);
                    self.registers.set16(Register16::PC, addr);
                    12
                } else {8}
            }
            0x30 => { // JR NC, r8
                let c = self.registers.get_flag(Flag::C);
                if !c {
                    let addr = self.registers.get16(Register16::PC).wrapping_sub(1);
                    let offset = memory.read_byte(self.registers.bump_pc()) as i8;
                    let addr = addr.wrapping_add(offset as u16);
                    self.registers.set16(Register16::PC, addr);
                    12
                } else {8}
            }

            // These instructions are illegal and hang the CPU
            0xD3 | 0xDB | 0xDD | 0xE3 | 0xE4 | 0xEB | 0xEC | 0xED | 0xF4 | 0xFC | 0xFD => {
                panic!("Illegal instruction!");
            },

            _ => unimplemented!("unimplemented instruction {:X} at {:X}!", instruction, self.registers.get16(Register16::PC))
        }
    }
}