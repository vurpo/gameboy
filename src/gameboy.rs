use crate::cpu::*;
use crate::gpu::*;
use crate::memory::*;

use std::sync::{
    Arc,
    Mutex,
};

pub enum GameBoyState {
    Running,
    Stopped,
}

#[derive(Default)]
pub struct Clock {
    pub t: u64,
    pub lastT: u64
}

impl Clock {
    pub fn increment(&mut self, t: u64) {
        self.lastT = t;
        self.t += t;
    }
}

pub struct GameBoy {
    pub cpu: CPU,
    pub gpu: GPU,
    pub clock: Arc<Mutex<Clock>>,
    pub memory: Arc<Mutex<Memory>>,
}

impl GameBoy {
    pub fn new() -> GameBoy {
        let memory = Arc::new(Mutex::new(Memory::new()));
        let clock = Arc::new(Mutex::new(Default::default()));
        GameBoy {
            cpu: CPU::new(memory.clone(), clock.clone()),
            gpu: GPU::new(memory.clone(), clock.clone()),
            clock,
            memory: memory
        }
    }

    pub fn load_rom(&mut self, rom: [u8; 0x8000]) {
        let mut memory = self.memory.lock().unwrap();
        memory.load_rom(rom);
    }

    pub fn reset(&mut self) {
        {
            let mut memory = self.memory.lock().unwrap();
            let mut clock = self.clock.lock().unwrap();
            *memory = Memory::new();
            *clock = Default::default();
        }
        self.cpu = CPU::new(self.memory.clone(), self.clock.clone());
        self.gpu = GPU::new(self.memory.clone(), self.clock.clone());
        self.cpu.set_pc(0x0100);
    }

    pub fn step(&mut self) -> GameBoyState {
        if let CPUState::Running = self.cpu.running_state {
            let cpustate = self.cpu.step_forward();
            self.gpu.step();
            
            match cpustate {
                CPUState::Running => GameBoyState::Running,
                _ => GameBoyState::Stopped
            }
        } else { GameBoyState::Stopped }
    }
}