use crate::memory::*;
use crate::gameboy::*;

use std::sync::{
    Arc,
    Mutex,
};

pub enum GPUMode {
    OAM,
    VRAM,
    HBlank,
    VBlank
}

pub struct GPU {
    pub mode: GPUMode,
    pub memory: Arc<Mutex<Memory>>,
    pub clock: Arc<Mutex<Clock>>,
    pub modeclock: u64,
    pub line: u64,
}

impl GPU {
    pub fn new(memory: Arc<Mutex<Memory>>, clock: Arc<Mutex<Clock>>) -> GPU {
        GPU {
            mode: GPUMode::HBlank,
            memory: memory,
            clock: clock,
            modeclock: 0,
            line: 0,
        }
    }

    pub fn step(&mut self) {
        {
            let clock = self.clock.lock().unwrap();
            self.modeclock += clock.lastT;
        }
        match &self.mode {
            OAM => {
                if self.modeclock >= 80 {
                    self.modeclock = 0;
                    self.mode = GPUMode::VRAM;
                }
            },
            VRAM => {
                if self.modeclock >= 172 {
                    self.modeclock = 0;
                    self.mode = GPUMode::HBlank;

                    self.renderscan();
                }
            },
            HBlank => {
                if self.modeclock >= 204 {
                    self.modeclock = 0;
                    self.line += 1;

                    if self.line == 143 {
                        self.mode = GPUMode::VBlank;
                        // TODO: the screen is complete, display it
                    } else {
                        self.mode = GPUMode::OAM;
                    }
                }
            },
            VBlank => {
                if self.modeclock >= 456 {
                    self.modeclock = 0;
                    self.line += 1;
                    
                    if self.line > 153 {
                        self.mode = GPUMode::OAM;
                        self.line = 0;
                    }
                }
            }
        }
    }

    pub fn renderscan(&mut self) {

    }
}