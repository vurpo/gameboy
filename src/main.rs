#![allow(non_snake_case)]

use std::io::Read;
use std::sync::{
    Arc,
    Mutex,
};

use crossbeam_utils::thread;

mod register;
mod memory;
mod cpu;
mod gpu;
mod gameboy;
mod io;
mod sdl_ui;

fn main() {
    let gameboy = Arc::new(Mutex::new(gameboy::GameBoy::new()));

    if let Ok(mut file) = std::fs::File::open("testrom.gb") {
        let mut rom: [u8; 0x8000] = [0; 0x8000];
        file.read_exact(&mut rom).unwrap();

        {
            let mut gameboy = gameboy.lock().unwrap();
            gameboy.reset();
            gameboy.load_rom(rom);
        }

        thread::scope(|s| {
            let gameboy_ = gameboy.clone();
            s.spawn(move |_| {
                loop {
                    let mut gameboy = gameboy_.lock().unwrap();
                    match gameboy.step() {
                        gameboy::GameBoyState::Stopped => break,
                        _ => ()
                    }
                }
            });

            let gameboy__ = gameboy.clone();
            s.spawn(move |_| {
                sdl_ui::main(gameboy__);
            });
        }).unwrap();
    }
}