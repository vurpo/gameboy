use crate::io::*;

enum Address {
    RAM(u16),
    MMIO(u16),
    BIOS(u16)
}

#[repr(C)]
pub struct Memory {
    cartridge_rom:      [u8; 0x4000], // 0x0000 - 0x3FFF
    cartridge_banks:    [u8; 0x4000], // 0x4000 - 0x7FFF
    graphics_ram:       [u8; 0x2000], // 0x8000 - 0x9FFF
    cartridge_ram:      [u8; 0x2000], // 0xA000 - 0xBFFF
    working_ram:        [u8; 0x2000], // 0xC000 - 0xDFFF
    shadow_ram:         [u8; 0x1E00], // 0xE000 - 0xFDFF
    sprite_ram:         [u8; 0x100],  // 0xFE00 - 0xFE9F (!)
    mmio:               [u8; 0x80],   // 0xFF00 - 0xFF7F
    zero_page:          [u8; 0x80],   // 0xFF80 - 0xFFFF

    io: IO,

    bios_mapped: bool
}

impl Memory {
    
    pub fn new() -> Memory {
        Memory {
            cartridge_rom: [0; 0x4000],
            cartridge_banks: [0; 0x4000],
            graphics_ram: [0; 0x2000],
            cartridge_ram: [0; 0x2000],
            working_ram: [0; 0x2000],
            shadow_ram: [0; 0x1E00],
            sprite_ram: [0; 0x100],
            mmio: [0; 0x80],
            zero_page: [0; 0x80],

            io: IO{},

            bios_mapped: false
        }
    }

    pub fn load_rom(&mut self, rom: [u8; 0x8000]) {
        self.cartridge_rom.clone_from_slice(&rom[0..0x4000]);
        self.cartridge_banks.clone_from_slice(&rom[0x4000..0x8000]);
    }

    fn map_addr(&self, addr: u16) -> Address {
        if addr&0xFF00 == 0 && self.bios_mapped {
            Address::BIOS(addr)
        } else if addr >= 0xE000 && addr <= 0xFDFF {
            // shadow RAM mirrors working RAM
            Address::RAM(addr - 0x2000)
        } else if addr >= 0xFF00 && addr <= 0xFF7F {
            // MMIO address
            Address::MMIO(addr)
        } else if addr >= 0xFF80 {
            // zero-page
            Address::RAM(addr & 0x7F)
        } else {
            Address::RAM(addr)
        }
    }

    // TODO: implement MMIO
    fn read_mmio_byte(&self, addr: u16) -> u8 {0}

    fn write_mmio_byte(&mut self, addr: u16, data: u8) {}

    fn read_ram_byte(&self, addr: u16) -> u8 {
        unsafe {
            // cast self into a raw pointer to a [u8; 0x10000] and index by `addr`
            (*(self as *const _ as *const [u8; 0x10000]))[addr as usize]
        }
    }

    fn write_ram_byte(&mut self, addr: u16, data: u8) {
        unsafe {
            // cast self into a raw pointer to a [u8; 0x10000] and index by `addr`
            (*(self as *mut _ as *mut [u8; 0x10000]))[addr as usize] = data;
        }
    }

    pub fn read_byte(&self, addr: u16) -> u8 {
        let addr = self.map_addr(addr);
        match addr {
            Address::RAM(addr) => self.read_ram_byte(addr),
            Address::MMIO(addr) => self.read_mmio_byte(addr),
            Address::BIOS(addr) => unimplemented!("mapped BIOS read")
        }
    }

    pub fn read_word(&self, addr: u16) -> u16 {
        self.read_byte(addr) as u16 + ((self.read_byte(addr+1) as u16)<<8)
    }

    pub fn write_byte(&mut self, addr: u16, data: u8) {
        let addr = self.map_addr(addr);
        match addr {
            Address::RAM(addr) => self.write_ram_byte(addr, data),
            Address::MMIO(addr) => self.write_mmio_byte(addr, data),
            Address::BIOS(addr) => ()
        }
    }

    pub fn write_word(&mut self, addr: u16, data: u16) {
        self.write_byte(addr, (data&0xFF) as u8);
        self.write_byte(addr+1, (data>>8) as u8);
    }
}