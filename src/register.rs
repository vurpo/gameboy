#[repr(C)]
pub union Register {
    f8: (u8, u8),
    f16: u16
}

pub struct Registers {
    AF: Register,
    BC: Register,
    DE: Register,
    HL: Register,
    SP: u16,
    PC: u16
}

pub enum FlagCondition {
    Clear,
    Set,
    If(bool),
}

pub enum Register8 { A,B,C,D,E,F,H,L }
pub enum Register16 { AF,BC,DE,HL,SP,PC }
pub enum Flag { Z,N,H,C }

impl Registers {
    pub fn new() -> Registers {
        Registers {
            AF: Register { f16: 0 },
            BC: Register { f16: 0 },
            DE: Register { f16: 0 },
            HL: Register { f16: 0 },
            SP: 0,
            PC: 0x100
        }
    }

    pub fn bump_pc(&mut self) -> u16 {
        let pc = self.PC;
        self.PC += 1;
        pc
    }

    pub fn bump_pc_word(&mut self) -> u16 {
        let pc = self.PC;
        self.PC += 2;
        pc
    }

    // Unsafe is required for union access,
    // although this usage is perfectly safe
    // (modifying the 8-bit fields cannot produce
    // an invalid value in the 16-bit field, or vice versa)

    #[inline]
    pub fn get8(&self, register: Register8) -> u8 {
        unsafe {
            use self::Register8::*;
            match register {
                A => self.AF.f8.0,
                B => self.BC.f8.0,
                C => self.BC.f8.1,
                D => self.DE.f8.0,
                E => self.DE.f8.1,
                F => self.AF.f8.1,
                H => self.HL.f8.0,
                L => self.HL.f8.1
            }
        }
    }

    #[inline]
    pub fn get16(&self, register: Register16) -> u16 {
        unsafe {
            use self::Register16::*;
            match register {
                AF => self.AF.f16,
                BC => self.BC.f16,
                DE => self.DE.f16,
                HL => self.HL.f16,
                SP => self.SP,
                PC => self.PC
            }
        }
    }

    #[inline]
    pub fn set8(&mut self, register: Register8, data: u8) {
        unsafe {
            use self::Register8::*;
            match register {
                A => { self.AF.f8.0 = data; },
                B => { self.BC.f8.0 = data; },
                C => { self.BC.f8.1 = data; },
                D => { self.DE.f8.0 = data; },
                E => { self.DE.f8.1 = data; },
                F => { self.AF.f8.1 = data; },
                H => { self.HL.f8.0 = data; },
                L => { self.HL.f8.1 = data; }
            }
        }
    }

    #[inline]
    pub fn set16(&mut self, register: Register16, data: u16) {
        use self::Register16::*;
        match register {
            AF => { self.AF.f16 = data; },
            BC => { self.BC.f16 = data; },
            DE => { self.DE.f16 = data; },
            HL => { self.HL.f16 = data; },
            SP => { self.SP = data; },
            PC => { self.PC = data; }
        }
    }

    #[inline]
    pub fn set_flag(&mut self,
                flag: Flag,
                condition: FlagCondition) {
        unsafe {
            let mask: u8 = match flag {
                Flag::Z => 0b01111111,
                Flag::N => 0b10111111,
                Flag::H => 0b11011111,
                Flag::C => 0b11101111
            };
            match condition {
                FlagCondition::If(false) |
                FlagCondition::Clear => self.AF.f8.1 &= mask,
                FlagCondition::If(true) |
                FlagCondition::Set =>   self.AF.f8.1 |= !mask
            }
        }
    }

    #[inline]
    pub fn get_flag(&self, flag: Flag) -> bool {
        unsafe {
            match flag {
                Flag::Z => 0b10000000&self.AF.f8.1 != 0,
                Flag::N => 0b01000000&self.AF.f8.1 != 0,
                Flag::H => 0b00100000&self.AF.f8.1 != 0,
                Flag::C => 0b00010000&self.AF.f8.1 != 0
            }
        }
    }
}